#ifndef _SLEEP_H_
#define _SLEEP_H_

void sleep_s(char secs);
void sleep_ms100(char ms100);
void sleep_ms10(char ms10);

#endif /*_SLEEP_H_*/
