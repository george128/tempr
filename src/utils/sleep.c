#include <timer/timer.h>
#include "sleep.h"

static void timer_callback(unsigned char timer_id, void *data)
{
    timer_free(timer_id);
    *(char *)data=1;
}

void sleep_s(char secs)
{
    char done = 0;
    struct timer t = {
        .period=((unsigned short)secs)*1000,
        .data=&done,
        .callback=timer_callback
    };
    timer_setup(&t);
    while(!done)
        timer_task();
}

void sleep_ms100(char ms100)
{
    char done = 0;
    struct timer t = {
        .period=((unsigned short)ms100)*100,
        .data=&done,
        .callback=timer_callback
    };
    timer_setup(&t);
    while(!done)
        timer_task();
}

void sleep_ms10(char ms10)
{
    char done = 0;
    struct timer t = {
        .period=((unsigned short)ms10)*10,
        .data=&done,
        .callback=timer_callback
    };
    timer_setup(&t);
    while(!done)
        timer_task();
}

