#include <utils/strconv.h>
#include <platform/utils.h>

#include <string.h>

static const char PROGMEM hex[]="0123456789ABCDEF";

char* byte2str(char byte, char *str)
{
    str[2]=0;
    str[0]= read_rom(hex+((byte>>4)&0xf));
    str[1]= read_rom(hex+(byte&0xf));
    return str;
}

char str2byte(char *str) {
    char b1=-1,b2=-1;
    int i;
    for(i=0;i<16;i++) {
        char ch=read_rom(hex+i);
        if(ch==str[0])
            b1=i;
        if(ch==str[1])
            b2=i;
    }
    return b1*16+b2;
}

char byte2char(char byte) {
    return read_rom(hex+byte);
}

char* byte2dec(char byte, char *str) {
    char d;
    if(byte < 0) {
        *(str++)='-';
        byte=-byte;
    }
    d=byte/100;
    if(d)
        *(str++)=byte2char(d);
    byte%=100;
    d=byte/10;
    if(d)
        *(str++)=byte2char(d);
    *(str++)=byte2char(byte%10);
    return str;
}

char* short2dec(short val, char *str)
{
    char i,len;
    char buf[7];
    if(val == 0) {
        str[0]='0';
        str[1]=0;
        return str;
    }
    if(val<0) {
        *(str++)='-';
        val=-val;
    }
    for(i=0; val>0; val/=10, i++) {
        buf[i]=byte2char(val%10);
    }
    len=i;
    for(i=0;i<len;i++)
        str[i]=buf[len-i-1];
    str[len]=0;
    return str;
}

char short_str_len(short val)
{
    char len=0;

    if(val == 0) {
        return 1;
    }
    if(val<0) {
        len++;
        val=-val;
    }
    for(; val>0; val/=10, len++);
    return len;
}


char* set_min_len(char *str, char min_len)
{
    char len,i;

    len=strlen(str);
    if(len<min_len) {
        char diff=min_len-len;
        memmove(str+diff, str, diff);
        for(i=0;i<diff;i++)
            str[i]='0';
    }
    return str;
}

static char* skip_zeroes(char *buf) {
    char *ptr = buf;
    while(*ptr=='0')ptr++;
    if(!*ptr)
        ptr--;
    return ptr;
}

char *byte10_2_str(char d, char *buf)
{
    buf[3]=0;
    for(char i=2; i>=0; i--) {
        buf[i]=byte2char(d%10);
        d/=10;
    }
    return skip_zeroes(buf);
}

char *short10_2_str(unsigned short d, char *buf)
{
    buf[5]=0;
    for(char i=4; i>=0; i--) {
        buf[i]=byte2char(d%10);
        d/=10;
    }
    return skip_zeroes(buf);
}

char *long10_2_str(unsigned long d, char *buf)
{
    buf[19]=0;
    for(char i=18; i>=0; i--) {
        buf[i]=byte2char(d%10);
        d/=10;
    }
    return skip_zeroes(buf);

}

char str10_2byte(const char *buf) {
    char val=0;
    char const *ptr=buf;

    while(*ptr) {
        if(*ptr=='-') {
            ptr++;
            continue;
        }
        val*=10;
        val+=(*(ptr++))-'0';
    }
    return *buf=='-'?-val:val;
}
