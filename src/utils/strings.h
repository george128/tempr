#ifndef _STRINGS_H_
#define _STRINGS_H_

#include <platform/utils.h>

#define get_str_arr(arr, i) ((PGM_P)pgm_read_word(&(arr[(unsigned)i])))

char get_max_arr_len_P(PGM_P const *arr, char arr_len);
char get_max_arr_len(char* const *arr, char arr_len);

char *find_quoted_str(char *buf);

#endif /*_STRINGS_H_*/
