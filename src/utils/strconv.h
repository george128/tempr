#ifndef _STRCONV_H_
#define _STRCONV_H_

char* byte2str(char byte, char *str);
char str2byte(char *str);

char byte2char(char byte);
char* byte2dec(char byte, char *str);
char* short2dec(short val, char *str);

char short_str_len(short val);
char* set_min_len(char *str, char min_len);

/*buf min 4 bytes*/
char *byte10_2_str(char d, char *buf);
/*buf min 6 bytes*/
char *short10_2_str(unsigned short d, char *buf);
/*buf min 20 bytes*/
char *long10_2_str(unsigned long d, char *buf);

char str10_2byte(const char *buf);

#endif /*_STRCONV_H_*/

