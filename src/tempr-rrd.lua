local socket = require "socket"
local rrd = require "rrd"

rrd_file = "temperatures.rrd";
rrd_img = "temperatures.png";

local ip_map = {
    ["192.168.254.134"]="temp1",
    ["192.168.254.138"]="temp2",
}


if arg[1] == "create" then
  rrd.create(rrd_file, "--step", "300", "DS:temp1:GAUGE:600:0:50", "DS:temp2:GAUGE:600:-10:50", "RRA:MAX:0.5:1:288")
  os.exit(0)
end

function fix_tempr(tempr)
    local sign, a, b = string.match(tempr,"(-*)(%d+).(%d+)")
--    print(tempr,"sign",sign,"a",a,"b",b)
    if tonumber(b) < 9 then
	b="0"..b
    end
    local val=sign..a.."."..b
    return val
end

local last_update=0

udp = socket.udp()
assert(udp:setsockname('*', 8888))
while true do
    local msg, ip, port = udp:receivefrom()
    local template = ip_map[ip]
    local val
    local t = os.date("*t")
    local time_str=("%d.%02d.%02d %02d:%02d:%02d"):format(t.year,t.month,t.day,t.hour,t.min,t.sec)

    val = tonumber(fix_tempr(msg)) or error("Could not cast '"..msg.."' to number.'")    
    io.write(time_str,": ", ip, ":", template, " -> ", val, "\n")

    rrd.update(rrd_file, "--template", template, "N:"..val)
    
    local now_time=os.time()
    if now_time - last_update > 600 then
	print("Generating "..rrd_img);
	rrd.graph(rrd_img, "-w", "785", "-h", "120", "-a", "PNG", "--slope-mode", "--start", "-604800", "--end", "now",
	    "--vertical-label", "temperature (°C)",
	    "DEF:temp1=temperatures.rrd:temp1:MAX",
	    "DEF:temp2=temperatures.rrd:temp2:MAX",
	    'LINE1:temp1#ff0000:"temp 1"',
	    'LINE1:temp2#0000ff:"temp 2"')
	last_update=now_time
    end
end