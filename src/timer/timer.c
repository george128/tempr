#include <timer/timer.h>
#include <platform/systime.h>
#include <platform/utils.h>
#include <console/console.h>
#include <utils/strings.h>

#define TIMER_DEBUG 1

#ifdef TIMER_DEBUG
PROGMEM static const char STR_TIMER_CALLBACK[]="timer callback ";
PROGMEM static const char STR_TIMER_SETUP[]="timer setup: ";
PROGMEM static const char STR_TIMER_FREE[]="timer free: ";
#endif

PROGMEM static const char STR_TIMER_NO_SLOTS[]="No free slot for new timer!!!\n";
PROGMEM static const char STR_TIMER_WRONG_ID[]="timer_free: wrong timer_id ";


static struct timer *timer_pool[TIMER_POOL_SIZE];

unsigned char timer_setup(struct timer *t)
{
    unsigned char i;

    for(i=0;i<TIMER_POOL_SIZE;i++) {
        if(timer_pool[i]==0)
            break;
    }

    if(i==TIMER_POOL_SIZE) {
        console_puts_P(STR_TIMER_NO_SLOTS);
        return -1;
    }
    timer_pool[i]=t;
    t->last_time=get_systime();
#if TIMER_DEBUG > 0
    console_put_msg(STR_TIMER_SETUP, i);
#endif
    return i;
}

void timer_free(unsigned char timer_id)
{
    if(timer_id>TIMER_POOL_SIZE 
       || timer_pool[timer_id]==NULL) {
        console_put_msg(STR_TIMER_WRONG_ID, timer_id);
        return;
    }
    timer_pool[timer_id]=NULL;
#if TIMER_DEBUG > 0
    console_put_msg(STR_TIMER_FREE, timer_id);
#endif
}

struct timer *timer_get(unsigned char timer_id)
{
    if(timer_id>TIMER_POOL_SIZE 
       || timer_pool[timer_id]==NULL) {
        console_put_msg(STR_TIMER_WRONG_ID, timer_id);
        return NULL;
    }
    return timer_pool[timer_id];
}

void timer_task()
{
    unsigned char i;
    unsigned short time=get_systime();
    unsigned short next_time;
    char is_overflow;

    for(i=0;i<TIMER_POOL_SIZE;i++) {
        struct timer *t=timer_pool[i];

        if(t!=NULL) {
            next_time=t->last_time+t->period;
            is_overflow=t->last_time>next_time;
            if(!is_overflow && next_time < time) {
                next_time = time;
            }
            if((!is_overflow && next_time<=time)
               ||(is_overflow && next_time<=time && t->last_time>time)) {
/*                debug_print("timer[%u]: time=%u callback\n",i,time); */
#if TIMER_DEBUG > 1
                console_puts_P(STR_TIMER_CALLBACK);
                console_putd(i);
                console_putc('-');
                console_putmem(&t->callback, 4);
                console_next_line();
#endif
                t->callback(i, t->data);
                t->last_time+=t->period;
            }
        }
    }
}
