#include <console/console.h>
#include "soft_uart.h"

void console_platform_init()
{
    char uart_no=soft_uart_register();
    register_console(uart_no);
}
