#ifndef _PLATFORM_UTILS_H_
#define _PLATFORM_UTILS_H_

#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <util/delay.h>

#define read_rom(a) pgm_read_byte(a)

#define debug_print(a, ...) 
#define debug_puts(a)

#ifndef NULL
#define NULL ((void*)0)
#endif /*NULL*/

#define eeprom_register_block(src, size) 

/*check struct size on compile stage*/
#define ct_assert(e) extern char (*ct_assert(void)) [sizeof(char[1 - 2*!(e)])]

#endif /*_PLATFORM_UTILS_H_*/
