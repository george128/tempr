#include <avr/io.h>
#include <platform/utils.h>
#include <console/console.h>
#include "spi.h"

#define SPI_DDR DDRB
#define SPI_PORT PORTB
#define SPI_MISO PB4
#define SPI_MOSI PB3
#define SPI_SCK PB5
#define SPI_SS PB2

#define SPI_DEBUG 0


#if SPI_DEBUG > 0
PROGMEM static const char STR_SEND_RECEIVE_START[] = "spi: send-receive start: ";
PROGMEM static const char STR_SEND_RECEIVE_DONE[] = "spi: send-receive done: ";
PROGMEM static const char STR_SEND_RECEIVE_SPCR[] = "spi: SPCR: ";
#if SPI_DEBUG >1
PROGMEM static const char STR_SEND_RECEIVE_SPSR[] = "spi: SPSR: ";
#endif
#endif

void spi_init(unsigned char clk_pol, unsigned char clk_pha)
{
#if SPI_DEBUG > 0
    char val;
#endif
    SPI_DDR &= ~(_BV(SPI_MISO) | _BV(SPI_SS)); /*inputs*/
    SPI_DDR |= (_BV(SPI_MOSI) | _BV(SPI_SCK)); /*outputs*/

    SPCR = _BV(SPE)               /* SPI Enable */
           |_BV(MSTR)             /* Master select */
           |_BV(SPR0)
           |_BV(SPR1);            /* speed: fosc/128 */

    if(clk_pol)
        SPCR |=_BV(CPOL);
    else
        SPCR &=~_BV(CPOL);

    if(clk_pha)
        SPCR |=_BV(CPHA);
    else
        SPCR &=~_BV(CPHA);
#if SPI_DEBUG > 0
    val = SPCR;
#endif
    SPI_PORT |= _BV(SPI_MOSI)
        |_BV(SPI_SCK)
        |_BV(SPI_SS)
        |_BV(SPI_MISO);
#if SPI_DEBUG > 0
    console_put_msg(STR_SEND_RECEIVE_SPCR, val);
#endif
}

unsigned char spi_send_receive(unsigned char wb)
{
    unsigned char val;
#if SPI_DEBUG >0
    console_puts_P(STR_SEND_RECEIVE_START);
    console_putd(wb);
    console_putc(':');
#endif
    SPDR = wb;
    
    do {
        val = SPSR;
#if SPI_DEBUG > 1
        console_put_msg(STR_SEND_RECEIVE_SPSR, val);
#endif
    } while(!(val & _BV(SPIF)));

    val = SPDR;
#if SPI_DEBUG > 0
    console_putd(val);
    console_next_line();
#endif
    return val;
}
