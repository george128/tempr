#ifndef _SOFT_UART_H_
#define _SOFT_UART_H_

#include <avr/io.h>

//Set tx to PB2 / rx to PB1
#define UART_Port _SFR_IO_ADDR(PORTD)
#define UART_Tx 7
#define UART_Rx 6

//Define baudrate
#define BAUD_RATE 38400

//Calculate delays
#ifdef F_CPU
#define TXDELAY (((F_CPU/BAUD_RATE)-7 +1.5)/3)
#define RXDELAY (((F_CPU/BAUD_RATE)-5 +1.5)/3)
#else
  #error CPU frequency F_CPU undefined
#endif

char soft_uart_register();

#endif /*_SOFT_UART_H_*/
