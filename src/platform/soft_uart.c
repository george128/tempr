#include <uart/uart.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

#include "soft_uart.h"

static char last_char;

static void soft_uart_putc(char ch) 
{
	uint8_t txdelay = TXDELAY;
	uint8_t delayCount = 0;

	__asm__ __volatile__(
		"0: cli\n\t"
		"   sbi %[uart_port]-1,%[uart_tx]\n\t"
		"   cbi %[uart_port],%[uart_tx]\n\t"
		"   in __tmp_reg__,%[uart_port]\n\t"
		"   ldi r25,3\n\t"
		"1: mov %[delayCount],%[txdelay]\n\t"
		"2: dec %[delayCount]\n\t"
		"   brne 2b\n\t"
		"   bst %[ch],0\n\t"
		"   bld __tmp_reg__,%[uart_tx]\n\t"
		"   lsr r25\n\t"
		"   ror %[ch]\n\t"
		"   out %[uart_port],__tmp_reg__\n\t"
		"   brne 1b\n\t"
		"   sei\n\t"
		:
		  [ch] "+r" (ch),
		  [delayCount] "+r" (delayCount)
		:
		  [uart_port] "M" (UART_Port),
		  [uart_tx] "M" (UART_Tx),
		  [txdelay] "r" (txdelay)
		:
		  "r25"
	);
}

static char soft_uart_getc() 
{
	uint8_t rxdelay = RXDELAY;
	uint8_t rxdelay15 = (RXDELAY*1.5)-2.5;
	uint8_t delayCount = 0;

	if(last_char!=0 && last_char!=-1) {
	    char tmp = last_char;
	    last_char=0;
	    return tmp;
	}

	__asm__ __volatile__(
		"0: cbi %[uart_port]-1,%[uart_rx]\n\t"
		"   sbi %[uart_port],%[uart_rx]\n\t"
		"   mov %[delayCount],%[rxdelay15]\n\t"
		"   ldi %[rxdelay15],0x80\n\t"
		"1: sbic %[uart_port]-2,%[uart_rx]\n\t"
		"   rjmp 4f\n\t"
		"   cli\n\t"
		"2: subi %[delayCount], 1\n\t"
		"   brne 2b\n\t"
		"   mov %[delayCount],%[rxdelay]\n\t"
		"   sbic %[uart_port]-2,%[uart_rx]\n\t"
		"   sec\n\t"
		"   ror %[rxdelay15]\n\t"
		"   brcc 2b\n\t"
		"3: dec %[delayCount]\n\t"
		"   brne 3b\n\t"
		"   sei\n\t"
        "   rjmp 5f\n\t"
        "4: ldi %[rxdelay15],-1\n\t"
        "5: \n\t"
	:
	  [rxdelay15] "+r" (rxdelay15),
	  [delayCount] "+r" (delayCount)
	:
	  [uart_port] "M" (UART_Port),
	  [uart_rx] "M" (UART_Rx),
	  [rxdelay] "r" (rxdelay)
	);
	return rxdelay15;
}

static char soft_uart_input_check(char timeout_ms) 
{
    last_char=soft_uart_getc();
    return last_char;
}

static void soft_uart_init() {}

PROGMEM static const char STR_SOFT_UART[]="soft_uart";
PROGMEM static const struct uart_dev soft_uart_dev =
{
    .init=soft_uart_init,
    .putc=soft_uart_putc,
    .readc=soft_uart_getc,
    .input_check=soft_uart_input_check,
    .name=STR_SOFT_UART,
};

char soft_uart_register()
{
    last_char=0;
    return register_uart(&soft_uart_dev);
}

