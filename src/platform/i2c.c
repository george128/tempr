#include <util/twi.h>

#include <platform/i2c.h>

void i2c_init()
{
    TWSR = 0;
    TWBR = (F_CPU / I2C_RATE - 16) / 2;
}

static char i2c_start(unsigned char addr)
{
    unsigned char status;

    TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
    while (!(TWCR & _BV(TWINT))) ; /* wait for transmission */
    status = TW_STATUS & TW_STATUS_MASK;
    if( status != TW_START && status != TW_REP_START)
        return 1;

    TWDR = addr;
    TWCR = _BV(TWINT) | _BV(TWEN);
    while(!(TWCR & _BV(TWINT)));

    status = TW_STATUS & TW_STATUS_MASK;
    if(status !=TW_MT_SLA_ACK && status != TW_MR_SLA_ACK)
        return 1;

    return 0;
}

static void i2c_start_wait(unsigned char addr)
{
    unsigned char status;

    for(;;) {
        TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
        while (!(TWCR & _BV(TWINT))) ; /* wait for transmission */

        status = TW_STATUS & TW_STATUS_MASK;
        if( status != TW_START && status != TW_REP_START)
            continue;

        TWDR = addr;
        TWCR = _BV(TWINT) | _BV(TWEN);
        while(!(TWCR & _BV(TWINT)));

        status = TW_STATUS & TW_STATUS_MASK;
        if(status == TW_MT_SLA_NACK && status == TW_MR_DATA_NACK) {
            TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);
            while(TWCR & _BV(TWSTO));
            continue;
        }

        break;
    }
}

static unsigned char i2c_read_ack()
{
    TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWEA);
    while(!(TWCR & _BV(TWINT)));
    return TWDR;
}

static unsigned char i2c_read_nack()
{
    TWCR = _BV(TWINT) | _BV(TWEN);
    while(!(TWCR & _BV(TWINT)));
    return TWDR;
}



static void i2c_stop()
{
    TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN); /* send stop condition */
    while(TWCR & _BV(TWSTO));
}

static unsigned char i2c_send_write(unsigned char data)
{
    unsigned char status;

    TWDR = data | TW_WRITE;
    TWCR = _BV(TWINT) | _BV(TWEN); /* clear interrupt to start transmission */
    while (!(TWCR & _BV(TWINT))) ; /* wait for transmission */
    status = TW_STATUS & TW_STATUS_MASK;
    if(status != TW_MT_DATA_ACK)
        return 1;
    return 0;
}

unsigned char i2c_read(unsigned char dev_no, 
                       unsigned char addr, 
                       unsigned char cnt, 
                       char *buf)
{
    unsigned char i;
    if(i2c_start(dev_no|TW_WRITE)) {
        i2c_stop();
        return -1;
    }
 
    i2c_send_write(addr);
    i2c_start(dev_no|TW_READ);
    for(i=0;i<cnt;i++) {
        *buf++=
            (i == cnt-1 ? i2c_read_nack() : i2c_read_ack());
    }

    i2c_stop();
    return cnt;
}
unsigned char i2c_write(unsigned char dev_no, 
                       unsigned char addr, 
                       unsigned char cnt, 
                       char *buf)
{
    unsigned char i;
    if(i2c_start(dev_no|TW_WRITE)) {
        i2c_stop();
        return -1;
    }
 
    i2c_send_write(addr);

    for(i=0;i<cnt;i++) {
        if(i2c_send_write(*(buf++))) {
            i2c_stop();
            return i;
        }
    }

    i2c_stop();
    return cnt;
}
