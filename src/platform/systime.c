#include <avr/io.h>
#include <avr/interrupt.h>
#include <platform/systime.h>

static unsigned short systime_cnt;

ISR(TIMER1_OVF_vect) {
    TCNT1 = 65535-16000;
    systime_cnt++;
}

void systime_init()
{
    systime_cnt = 0;
    TIMSK1 = _BV(TOIE1); /*timer1 overflow interrupt enable */
    TCCR1B = _BV(CS10); /*timer1 16 bit no clk prescaling */
    sei(); /*enable global interrupts*/
}


/*Get system time in ms*/
unsigned short get_systime()
{
    return systime_cnt;
}
