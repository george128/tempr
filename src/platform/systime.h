#ifndef _SYSTIME_H_
#define _SYSTIME_H_

void systime_init();

/*Get system time in ms*/
unsigned short get_systime();

#endif /*_SYSTIME_H_*/
