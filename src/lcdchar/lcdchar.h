#ifndef _LCDCHAR_H_
#define _LCDCHAR_H_

void lcd_char_init();
void lcd_char_clear();
void lcd_char_home();
void lcd_char_no_display();
void lcd_char_display();
void lcd_char_no_blink();
void lcd_char_blink();
void lcd_char_no_cursor();
void lcd_char_cursor();
void lcd_char_scroll_display_left();
void lcd_char_scroll_display_right();
void lcd_char_left_to_right();
void lcd_char_right_to_left();
void lcd_char_no_backlight();
void lcd_char_backlight();
void lcd_char_autoscroll();
void lcd_char_no_autoscroll(); 
void lcd_char_create_char(unsigned char location, const char* charmap);
void lcd_char_set_cursor(unsigned char col, unsigned char row); 
void lcd_char_write(char ch);
void lcd_char_write_str(const char *str);
void lcd_char_write_str_P(const char *str);



#endif /*_LCDCHAR_H_*/
