#include <platform/i2c.h>
#include <platform/utils.h>
#include <platform/delay.h>
#include "lcdchar.h"

#define LCD_ADDR 0x4e
#define LCD_ROWS 2
#define LCD_COLS 16

// commands
#define LCD_CLEARDISPLAY 0x01
#define LCD_RETURNHOME 0x02
#define LCD_ENTRYMODESET 0x04
#define LCD_DISPLAYCONTROL 0x08
#define LCD_CURSORSHIFT 0x10
#define LCD_FUNCTIONSET 0x20
#define LCD_SETCGRAMADDR 0x40
#define LCD_SETDDRAMADDR 0x80

// flags for display entry mode
#define LCD_ENTRYRIGHT 0x00
#define LCD_ENTRYLEFT 0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON 0x04
#define LCD_DISPLAYOFF 0x00
#define LCD_CURSORON 0x02
#define LCD_CURSOROFF 0x00
#define LCD_BLINKON 0x01
#define LCD_BLINKOFF 0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08
#define LCD_CURSORMOVE 0x00
#define LCD_MOVERIGHT 0x04
#define LCD_MOVELEFT 0x00

// flags for function set
#define LCD_8BITMODE 0x10
#define LCD_4BITMODE 0x00
#define LCD_2LINE 0x08
#define LCD_1LINE 0x00
#define LCD_5x10DOTS 0x04
#define LCD_5x8DOTS 0x00

// flags for backlight control
#define LCD_BACKLIGHT 0x08
#define LCD_NOBACKLIGHT 0x00

#define En _BV(2)  // Enable bit
#define Rw _BV(1)  // Read/Write bit
#define Rs _BV(0)  // Register select bit

static const PROGMEM unsigned char row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };


struct lcd_char {
    unsigned char dispfunc;
    unsigned char dispcntl;
    unsigned char dispmode;
    unsigned char backlightval;
};

static struct lcd_char state;

static inline void expanderWrite(unsigned char data) {
    i2c_write(LCD_ADDR, data|state.backlightval, 0, NULL);
}

static inline void pulseEnable(unsigned char data){
	expanderWrite(data | En);	// En high
	_delay_us(1);		        // enable pulse must be >450ns
	
	expanderWrite(data & ~En);	// En low
	_delay_us(50);		        // commands need > 37us to settle
} 

static void write4bits(unsigned char value) {
	expanderWrite(value);
	pulseEnable(value);
}

// write either command or data
static void send(unsigned char value, unsigned char mode) {
	unsigned char highnib=value&0xf0;
	unsigned char lownib=(value<<4)&0xf0;
	write4bits((highnib)|mode);
	write4bits((lownib)|mode); 
}

static inline void command(unsigned char value) {
	send(value, 0);
}

void lcd_char_init() {
    state.backlightval = LCD_BACKLIGHT;
    state.dispfunc = LCD_4BITMODE | LCD_2LINE | LCD_5x8DOTS;

	// SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
	// according to datasheet, we need at least 40ms after power rises above 2.7V
	// before sending commands. Arduino can turn on way befer 4.5V so we'll wait 50
	_delay_ms(50); 

	// Now we pull both RS and R/W low to begin commands
	expanderWrite(state.backlightval);	// reset expanderand turn backlight off (Bit 8 =1)
	_delay_ms(1000);

  	//put the LCD into 4 bit mode
	// this is according to the hitachi HD44780 datasheet
	// figure 24, pg 46
	
    // we start in 8bit mode, try to set 4 bit mode
    write4bits(0x03 << 4);
    _delay_ms(5); // wait min 4.1ms
   
    // second try
    write4bits(0x03 << 4);
    _delay_ms(5); // wait min 4.1ms
   
    // third go!
    write4bits(0x03 << 4); 
    _delay_us(150);
   
    // finally, set to 4-bit interface
    write4bits(0x02 << 4); 


	// set # lines, font size, etc.
	command(LCD_FUNCTIONSET | state.dispfunc);  
	
	// turn the display on with no cursor or blinking default
	state.dispcntl = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;
	lcd_char_display();
	
	// clear it off
	lcd_char_clear();
	
	// Initialize to default text direction (for roman languages)
	state.dispmode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
	
	// set the entry mode
	command(LCD_ENTRYMODESET | state.dispmode);
	
	lcd_char_home();
}

void lcd_char_clear() {
	command(LCD_CLEARDISPLAY);// clear display, set cursor position to zero
	_delay_ms(2);  // this command takes a long time!

}

void lcd_char_home() {
	command(LCD_RETURNHOME);  // set cursor position to zero
	_delay_ms(2);  // this command takes a long time!
}

void lcd_char_no_display() {
	state.dispcntl &= ~LCD_DISPLAYON;
	command(LCD_DISPLAYCONTROL | state.dispcntl);
}

void lcd_char_display() {
	state.dispcntl |= LCD_DISPLAYON;
	command(LCD_DISPLAYCONTROL | state.dispcntl);
}

void lcd_char_no_blink() {
	state.dispcntl &= ~LCD_BLINKON;
	command(LCD_DISPLAYCONTROL | state.dispcntl);
}

void lcd_char_blink() {
	state.dispcntl |= LCD_BLINKON;
	command(LCD_DISPLAYCONTROL | state.dispcntl);
}

void lcd_char_no_cursor() {
	state.dispcntl &= ~LCD_CURSORON;
	command(LCD_DISPLAYCONTROL | state.dispcntl);
}

void lcd_char_cursor() {
	state.dispcntl |= LCD_CURSORON;
	command(LCD_DISPLAYCONTROL | state.dispcntl);
}

void lcd_char_scroll_display_left() {
	command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}

void lcd_char_scroll_display_right() {
	command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

void lcd_char_left_to_right() {
	state.dispmode |= LCD_ENTRYLEFT;
	command(LCD_ENTRYMODESET | state.dispmode);
}

void lcd_char_right_to_left() {
	state.dispmode &= ~LCD_ENTRYLEFT;
	command(LCD_ENTRYMODESET | state.dispmode);
}

void lcd_char_no_backlight() {
	state.backlightval=LCD_NOBACKLIGHT;
	expanderWrite(0);
}

void lcd_char_backlight() {
	state.backlightval=LCD_BACKLIGHT;
	expanderWrite(0);

}

void lcd_char_autoscroll() {
	state.dispmode |= LCD_ENTRYSHIFTINCREMENT;
	command(LCD_ENTRYMODESET | state.dispmode);
}

void lcd_char_no_autoscroll() {
	state.dispmode &= ~LCD_ENTRYSHIFTINCREMENT;
	command(LCD_ENTRYMODESET | state.dispmode);
}

void lcd_char_create_char(unsigned char location, const char *charmap) {
	location &= 0x7; // we only have 8 locations 0-7
	command(LCD_SETCGRAMADDR | (location << 3));
	for (char i=0; i<8; i++) {
		lcd_char_write(charmap[i]);
	}
}

void lcd_char_set_cursor(unsigned char col, unsigned char row) {
	command(LCD_SETDDRAMADDR | (col + pgm_read_byte(row_offsets+row)));
}

void lcd_char_write(char ch) {
    send(ch, Rs);    
}

void lcd_char_write_str(const char *str) {
    char ch;
    while((ch=*str)) {
        if(ch=='\n')
            ch=' ';
        lcd_char_write(ch);
        str++;
    }
}

void lcd_char_write_str_P(const char *str) {
    char ch;
    while((ch=pgm_read_byte(str++))!=0) {
        lcd_char_write(ch);
    }
}

