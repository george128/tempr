#include <string.h>

#include <platform/spi.h>
#include <platform/utils.h>
#include <platform/spi.h>
#include <console/console.h>
#include "nrf24l01.h"

#define NRF_DEBUG 0

#define NRF24L01_CH 76
#define NRF24L01_ADDRSIZE 3
#define NRF24L01_RF24_PA 3

#define NRF_PORT PORTB
#define NRF_DDR DDRB
#define NRF_CE 1
#define NRF_CSN 0

/*auto ack enabled*/
#define NRF24L01_ACK

#define NRF24L01_REG_CONFIG      0x00
#define NRF24L01_REG_EN_AA       0x01
#define NRF24L01_REG_EN_RXADDR   0x02
#define NRF24L01_REG_SETUP_AW    0x03
#define NRF24L01_REG_SETUP_RETR  0x04
#define NRF24L01_REG_RF_CH       0x05
#define NRF24L01_REG_RF_SETUP    0x06
#define NRF24L01_REG_STATUS      0x07
#define NRF24L01_REG_OBSERVE_TX  0x08
#define NRF24L01_REG_CD          0x09
#define NRF24L01_REG_RX_ADDR_P0  0x0A
#define NRF24L01_REG_RX_ADDR_P1  0x0B
#define NRF24L01_REG_RX_ADDR_P2  0x0C
#define NRF24L01_REG_RX_ADDR_P3  0x0D
#define NRF24L01_REG_RX_ADDR_P4  0x0E
#define NRF24L01_REG_RX_ADDR_P5  0x0F
#define NRF24L01_REG_TX_ADDR     0x10
#define NRF24L01_REG_RX_PW_P0    0x11
#define NRF24L01_REG_RX_PW_P1    0x12
#define NRF24L01_REG_RX_PW_P2    0x13
#define NRF24L01_REG_RX_PW_P3    0x14
#define NRF24L01_REG_RX_PW_P4    0x15
#define NRF24L01_REG_RX_PW_P5    0x16
#define NRF24L01_REG_FIFO_STATUS 0x17
#define NRF24L01_REG_FEATURE     0x1D
#define NRF24L01_REG_DYNPD       0x1C

#define NRF24L01_CMD_R_REGISTER    0x00
#define NRF24L01_CMD_W_REGISTER    0x20
#define NRF24L01_CMD_REGISTER_MASK 0x1F
#define NRF24L01_CMD_R_RX_PAYLOAD  0x61
#define NRF24L01_CMD_W_TX_PAYLOAD  0xA0
#define NRF24L01_CMD_FLUSH_TX      0xE1
#define NRF24L01_CMD_FLUSH_RX      0xE2
#define NRF24L01_CMD_REUSE_TX_PL   0xE3
#define NRF24L01_CMD_NOP           0xFF

#define check_single_byte(a) ct_assert(sizeof(union a)==1)

union reg_config {
    struct {
        unsigned int prim_rx:1;
        unsigned int pwr_up:1;
        unsigned int crco:1;
        unsigned int en_crc:1;
        unsigned int max_rt:1;
        unsigned int tx_ds:1;
        unsigned int rx_dr:1;
        unsigned int unused:1;
    }  __attribute__ ((packed)) bits;
    unsigned char byte;
} __attribute__ ((packed));
check_single_byte(reg_config);

union reg_en_aa {
    struct nrf24l01_pipe bits;
    unsigned char byte;
} __attribute__ ((packed));
check_single_byte(reg_en_aa);

union reg_en_rxaddr {
    struct nrf24l01_pipe bits;
    unsigned char byte;
} __attribute__ ((packed));
check_single_byte(reg_en_rxaddr);

union reg_setup_aw {
    struct {
        unsigned int aw:2;
/* '00' - Illegal
   '01' - 3 bytes
   '10' - 4 bytes
   '11' - 5 bytes
*/
        unsigned int unused:6;
    } __attribute__ ((packed)) bits;
    unsigned char byte;
} __attribute__ ((packed));
check_single_byte(reg_setup_aw);

union reg_setup_retr {
    struct {
        unsigned int arc:4;
/* `0000' - Re-Transmit disabled
   `0001' - Up to 1 Re-Transmit on fail of AA
   ......
   `1111' - Up to 15 Re-Transmit on fail of AA
*/
        unsigned int ard:4;
/* 0000' - Wait 250 uS
   0001' - Wait 500 uS
   0010' - Wait 750 uS
   ........
   1111' - Wait 4000 uS
*/
    } __attribute__ ((packed)) bits;
    unsigned char byte;
} __attribute__ ((packed));
check_single_byte(reg_setup_retr);

union reg_rf_setup {
    struct {
        unsigned int lna_hcurr:1; /*obsoleted*/
        unsigned int rf_pwr:2;
        unsigned int rf_dr_high:1;
        unsigned int pll_lock:1;
        unsigned int rf_dr_low:1;
        unsigned int unused:1;
        unsigned int cont_wave:1;
    } __attribute__ ((packed)) bits;
    unsigned char byte;
} __attribute__ ((packed));
check_single_byte(reg_rf_setup);

union reg_status {
    struct {
        unsigned int tx_full:1;
        unsigned int rx_p_no:3;
        unsigned int max_rt:1;
        unsigned int tx_ds:1;
        unsigned int rx_dr:1;
        unsigned int unused:1;
    } __attribute__ ((packed)) bits;
    unsigned char byte;
} __attribute__ ((packed));
check_single_byte(reg_status);

union reg_observe_tx {
    struct {
        unsigned int arc_cnt:4;
        unsigned int plos_cnt:4;
    } __attribute__ ((packed)) bits;
    unsigned char byte;
} __attribute__ ((packed));
check_single_byte(reg_observe_tx);

union reg_rx_pw {
    struct {
        unsigned int rx_pw:6;
        unsigned int unused:2;
    } __attribute__ ((packed)) bits;
    unsigned char byte;
} __attribute__ ((packed));
check_single_byte(reg_rx_pw);

union reg_fifo_status {
    struct {
        unsigned int rx_empty:1;
        unsigned int rx_full:1;
        unsigned int unused2:2;
        unsigned int tx_empty:1;
        unsigned int tx_full:1;
        unsigned int tx_reuse:1;
        unsigned int unused1:1;
    } __attribute__ ((packed)) bits;
    unsigned char byte;
} __attribute__ ((packed));
check_single_byte(reg_fifo_status);

union reg_dynpld {
    struct nrf24l01_pipe bits;
    unsigned char byte;
} __attribute__ ((packed));
check_single_byte(reg_dynpld);

union reg_feature {
    struct {
        unsigned int en_dyn_ack:1;
        unsigned int en_ack_pay:1;
        unsigned int en_dpl:1;
        unsigned int unused:5;
    } __attribute__ ((packed)) bits;
    unsigned char byte;
} __attribute__ ((packed));
check_single_byte(reg_feature);

static inline void nrf_csn_hi() {
    NRF_PORT |= _BV(NRF_CSN);
}
static inline void nrf_csn_lo() {
    NRF_PORT &= ~_BV(NRF_CSN);
}
static inline void nrf_ce_hi() {
    NRF_PORT |= _BV(NRF_CE);
}
static inline void nrf_ce_lo() {
    NRF_PORT &= ~_BV(NRF_CE);
}

PROGMEM static const unsigned char pipes_regs[] = 
{
    NRF24L01_REG_RX_ADDR_P0,
    NRF24L01_REG_RX_ADDR_P1,
    NRF24L01_REG_RX_ADDR_P2,
    NRF24L01_REG_RX_ADDR_P3,
    NRF24L01_REG_RX_ADDR_P4,
    NRF24L01_REG_RX_ADDR_P5
};

PROGMEM static const unsigned char NRF24L01_ADDR_P0[] = {0xF0, 0x35, 0x77};
PROGMEM static const unsigned char NRF24L01_ADDR_P1[] = {0xC2, 0xC2, 0xC2};
PROGMEM static const unsigned char NRF24L01_ADDR_P2[] = {0xC2, 0xC2, 0xC3};
PROGMEM static const unsigned char NRF24L01_ADDR_P3[] = {0xC2, 0xC2, 0xC4};
PROGMEM static const unsigned char NRF24L01_ADDR_P4[] = {0xC2, 0xC2, 0xC5};
PROGMEM static const unsigned char NRF24L01_ADDR_P5[] = {0xC2, 0xC2, 0xC6};
PROGMEM static const unsigned char NRF24L01_ADDR_TX[] = {0xF0, 0x35, 0x77};

PROGMEM static const unsigned PGM_P const NRF24L01_ADDRS_RX[] = {
    NRF24L01_ADDR_P0,
    NRF24L01_ADDR_P1,
    NRF24L01_ADDR_P2,
    NRF24L01_ADDR_P3,
    NRF24L01_ADDR_P4,
    NRF24L01_ADDR_P5
};

#define PREFIX "nrf24l01: "

PROGMEM static const char STR_NRF_WRONG_PIPE[] = PREFIX "Wrong pipe no: ";
PROGMEM static const char STR_NRF_INITIALIZED[] = PREFIX "initialized\n";
PROGMEM static const char STR_NRF_TX_STAT[] = PREFIX "Tx stat (lost-retransmit) packets: ";
#if NRF_DEBUG > 0
PROGMEM static const char STR_NRF_READ_REG[] = PREFIX "read reg: ";
PROGMEM static const char STR_NRF_WRITE_REG[] = PREFIX "write reg: ";
PROGMEM static const char STR_NRF_READ_STATUS[] = PREFIX "get status: ";
PROGMEM static const char STR_NRF_SET_RX[] = PREFIX "set_rx\n";
PROGMEM static const char STR_NRF_SET_TX[] = PREFIX "set_tx\n";
PROGMEM static const char STR_NRF_SET_TX_ADDR[] = PREFIX "set_tx_addr: ";
PROGMEM static const char STR_NRF_SET_RX_ADDR[] = PREFIX "set_rx_addr: ";
PROGMEM static const char STR_NRF_ENABLE_PIPE[] = PREFIX "enable pipe: ";
#endif

static unsigned char addrs_rx[6][NRF24L01_ADDRSIZE];
static unsigned char addr_tx[NRF24L01_ADDRSIZE];

static unsigned char reg_read(unsigned char reg_no) {
    nrf_csn_lo();
    unsigned char data;
    spi_send_receive(NRF24L01_CMD_R_REGISTER 
                            | (reg_no & NRF24L01_CMD_REGISTER_MASK));
    data = spi_send_receive(NRF24L01_CMD_NOP);
    nrf_csn_hi();
#if NRF_DEBUG > 0
    console_puts_P(STR_NRF_READ_REG);
    console_putd(reg_no);
    console_putc(':');
    console_putd(data);
    console_putc('\n');
#endif
    return data;
}

/*static*/ void reg_read_seq(unsigned char reg_no, 
                                  unsigned char *buf, char len) {
    unsigned char *ptr, i;
    nrf_csn_lo();
    spi_send_receive(NRF24L01_CMD_R_REGISTER 
                     | (NRF24L01_CMD_REGISTER_MASK & reg_no));
    for(i=0, ptr=buf; i<len; i++, ptr++)
        *ptr = spi_send_receive(NRF24L01_CMD_NOP);
    nrf_csn_hi();
}

static void reg_write(unsigned char reg_no, unsigned char value) {
#if NRF_DEBUG > 0
    console_puts_P(STR_NRF_WRITE_REG);
    console_putd(reg_no);
    console_putc(':');
    console_putd(value);
    console_putc('\n');
#endif
    nrf_csn_lo();
    spi_send_receive(NRF24L01_CMD_W_REGISTER 
                     | (NRF24L01_CMD_REGISTER_MASK & reg_no));
    spi_send_receive(value);
    nrf_csn_hi();
}

static void reg_write_seq(unsigned char reg_no, 
                          unsigned char *buf, unsigned char len) {
    unsigned char i, *ptr;

#if NRF_DEBUG > 0
    console_puts_P(STR_NRF_WRITE_REG);
    console_putd(reg_no);
#endif
    nrf_csn_lo();
    spi_send_receive(NRF24L01_CMD_W_REGISTER 
                     | (NRF24L01_CMD_REGISTER_MASK & reg_no));
    for(i=0, ptr=buf; i<len; i++, ptr++) {
        spi_send_receive(*ptr);
#if NRF_DEBUG > 0
        console_putc(':');
        console_putd(*ptr);
#endif
    }
    nrf_csn_hi();
#if NRF_DEBUG > 0
    console_putc('\n');
#endif
}

static void reverse_address(unsigned char *addr, unsigned char *addr_rev) {
    /*reverse address*/
    unsigned char i;
    addr=addr+NRF24L01_ADDRSIZE-1;
    for(i=0; i<NRF24L01_ADDRSIZE; i++)
        *(addr_rev++)=*(addr--);
}

static void nrf24l01_status(union reg_status *status) {
    nrf_csn_lo();
    status->byte=spi_send_receive(NRF24L01_CMD_NOP);
    nrf_csn_hi();
#if NRF_DEBUG > 1
    console_put_msg(STR_NRF_READ_STATUS, status->byte);
#endif
}

static void flush_rx_fifo() {
    nrf_csn_lo();
    spi_send_receive(NRF24L01_CMD_FLUSH_RX);
    nrf_csn_hi();
}

static void flush_tx_fifo() {
    nrf_csn_lo();
    spi_send_receive(NRF24L01_CMD_FLUSH_TX);
    nrf_csn_hi();
}

static void set_rx() {
    union reg_config conf;
    union reg_status status;

#if NRF_DEBUG > 0
    console_puts_P(STR_NRF_SET_RX);
#endif
    /*nrf24l01_set_rx_addr(0, addrs_rx[0]); */ /*restore pipe 0 address*/
    conf.byte=reg_read(NRF24L01_REG_CONFIG);
    conf.bits.prim_rx=1; /*prx mode*/
    conf.bits.pwr_up=1; /*power up*/
    reg_write(NRF24L01_REG_CONFIG, conf.byte); 

    status.byte=0;
    status.bits.rx_dr=1;
    status.bits.tx_ds=1;
    status.bits.max_rt=1;
    reg_write(NRF24L01_REG_STATUS, status.byte); /*reset status*/
    flush_rx_fifo(); /*flush rx fifo*/
    flush_tx_fifo(); /*flush tx fifo*/
    nrf_ce_hi(); /*start listening*/
    _delay_us(150); /*wait for the radio to power up*/
}

static void set_tx() {
    union reg_config conf;
    union reg_status status;

#if NRF_DEBUG > 0
    console_puts_P(STR_NRF_SET_TX);
#endif
    nrf_ce_lo(); /*stop listening*/
    conf.byte = reg_read(NRF24L01_REG_CONFIG);

    conf.bits.prim_rx=0; /*ptx mode*/
    conf.bits.pwr_up=1; /*power up*/
    reg_write(NRF24L01_REG_CONFIG, conf.byte); 
    
    status.byte=0;
    status.bits.rx_dr=1;
    status.bits.tx_ds=1;
    status.bits.max_rt=1;
    reg_write(NRF24L01_REG_STATUS, status.byte); /*reset status*/
    flush_tx_fifo(); /*flush tx fifo*/
    _delay_us(150); /*wait for the radio to power up*/
}


static void set_pwr_level() {
    union reg_rf_setup setup;
    
    setup.byte=reg_read(NRF24L01_REG_RF_SETUP);
    if(setup.bits.rf_pwr != NRF24L01_RF24_PA) {
        setup.bits.rf_pwr=NRF24L01_RF24_PA;
        reg_write(NRF24L01_REG_RF_SETUP, setup.byte);
    }
}

static void set_rate() {
    union reg_rf_setup setup;
    
    setup.byte=reg_read(NRF24L01_REG_RF_SETUP);
    /*
      [RF_DR_LOW, RF_DR_HIGH]:
      '00' - 1Mbps
      '01' - 2Mbps
      '10' - 250kbps
      '11' - Reserved
     */
    /*250kbits*/
    setup.bits.rf_dr_low=0;
    setup.bits.rf_dr_high=1;
    reg_write(NRF24L01_REG_RF_SETUP, setup.byte);
}

static void set_crc() {
    union reg_config conf;
    
    conf.byte = reg_read(NRF24L01_REG_CONFIG);
    conf.bits.en_crc=1;
    conf.bits.crco=0; /*enable CRC 1 byte*/
    reg_write(NRF24L01_REG_CONFIG, conf.byte);
}

static void init_pipes(struct nrf24l01_pipe *pipes) {
    union reg_en_rxaddr en_rx={.byte=0};
    union reg_en_aa en_aa={.byte=0};

    if(pipes->p0) {
#if NRF_DEBUG > 0
        console_put_msg(STR_NRF_ENABLE_PIPE, 0);
#endif
        reg_write(NRF24L01_REG_RX_PW_P0, NRF24L01_PAYLOAD);
        en_rx.bits.p0=1;
#ifdef NRF24L01_ACK
        en_aa.bits.p0=1;
    }
#endif
    if(pipes->p1) {
#if NRF_DEBUG > 0
        console_put_msg(STR_NRF_ENABLE_PIPE, 1);
#endif
        reg_write(NRF24L01_REG_RX_PW_P1, NRF24L01_PAYLOAD);
        en_rx.bits.p1=1;
#ifdef NRF24L01_ACK
        en_aa.bits.p1=1;
    }
#endif
    if(pipes->p2) {
#if NRF_DEBUG > 0
        console_put_msg(STR_NRF_ENABLE_PIPE, 2);
#endif
        reg_write(NRF24L01_REG_RX_PW_P2, NRF24L01_PAYLOAD);
        en_rx.bits.p2=1;
#ifdef NRF24L01_ACK
        en_aa.bits.p2=1;
    }
#endif
    if(pipes->p3) {
#if NRF_DEBUG > 0
        console_put_msg(STR_NRF_ENABLE_PIPE, 3);
#endif
        reg_write(NRF24L01_REG_RX_PW_P3, NRF24L01_PAYLOAD);
        en_rx.bits.p3=1;
#ifdef NRF24L01_ACK
        en_aa.bits.p3=1;
    }
#endif
    if(pipes->p4) {
#if NRF_DEBUG > 0
        console_put_msg(STR_NRF_ENABLE_PIPE, 4);
#endif
        reg_write(NRF24L01_REG_RX_PW_P4, NRF24L01_PAYLOAD);
        en_rx.bits.p4=1;
#ifdef NRF24L01_ACK
        en_aa.bits.p4=1;
    }
#endif
    if(pipes->p5) {
#if NRF_DEBUG > 0
        console_put_msg(STR_NRF_ENABLE_PIPE, 5);
#endif
        reg_write(NRF24L01_REG_RX_PW_P5, NRF24L01_PAYLOAD);
        en_rx.bits.p5=1;
#ifdef NRF24L01_ACK
        en_aa.bits.p5=1;
    }
#endif
    reg_write(NRF24L01_REG_EN_RXADDR, en_rx.byte);
    reg_write(NRF24L01_REG_EN_AA, en_aa.byte);
}

#if NRF_DEBUG > 0
static void dump_regs() {
    unsigned char buf[5];

    reg_read(NRF24L01_REG_CONFIG);
    reg_read(NRF24L01_REG_EN_AA);
    reg_read(NRF24L01_REG_EN_RXADDR);
    reg_read(NRF24L01_REG_SETUP_AW);
    reg_read(NRF24L01_REG_SETUP_RETR);
    reg_read(NRF24L01_REG_RF_CH);
    reg_read(NRF24L01_REG_RF_SETUP);
    reg_read(NRF24L01_REG_STATUS);
    reg_read(NRF24L01_REG_OBSERVE_TX);
    reg_read(0x09); /*RPD*/
    reg_read_seq(NRF24L01_REG_RX_ADDR_P0, buf, 5);
    reg_read_seq(NRF24L01_REG_RX_ADDR_P1, buf, 5);
    reg_read_seq(NRF24L01_REG_RX_ADDR_P2, buf, 5);
    reg_read_seq(NRF24L01_REG_RX_ADDR_P3, buf, 5);
    reg_read_seq(NRF24L01_REG_RX_ADDR_P4, buf, 5);
    reg_read_seq(NRF24L01_REG_RX_ADDR_P5, buf, 5);
    reg_read_seq(NRF24L01_REG_TX_ADDR, buf, 5);
    reg_read(NRF24L01_REG_RX_PW_P0);
    reg_read(NRF24L01_REG_RX_PW_P1);
    reg_read(NRF24L01_REG_RX_PW_P2);
    reg_read(NRF24L01_REG_RX_PW_P3);
    reg_read(NRF24L01_REG_RX_PW_P4);
    reg_read(NRF24L01_REG_RX_PW_P5);
    reg_read(NRF24L01_REG_FIFO_STATUS);
    reg_read(NRF24L01_REG_DYNPD);
    reg_read(NRF24L01_REG_FEATURE);

}
#endif

char nrf24l01_init(struct nrf24l01_pipe *pipes, char repeats, char repeat_delay) {
    unsigned char i;
    union reg_setup_retr retr;

    /*setup port*/
    NRF_DDR |= _BV(NRF_CSN)|_BV(NRF_CE); /*outputs*/
    spi_init(0, 0);
    nrf_ce_lo(); /*low CE*/
    nrf_csn_hi(); /*high CSN*/

    _delay_ms(5); /*wait for the radio to init*/

#if NRF_DEBUG > 0
    dump_regs();
#endif
    set_pwr_level();
    set_rate();
    set_crc();

    retr.bits.ard=repeat_delay; /*uS*/
    retr.bits.arc=repeats; /*15 times*/
    reg_write(NRF24L01_REG_SETUP_RETR, retr.byte);

    reg_write(NRF24L01_REG_DYNPD, 0); /*disable dynamic payloads*/
    reg_write(NRF24L01_REG_RF_CH, NRF24L01_CH); /*set RF channel*/
    reg_write(NRF24L01_REG_SETUP_AW, NRF24L01_ADDRSIZE-2); /*set address width*/
    
    init_pipes(pipes);
    
    for(i=0;i<6;i++) {
        memcpy_P(addrs_rx[i], 
            (PGM_P)pgm_read_word(&(NRF24L01_ADDRS_RX[i])), 
            NRF24L01_ADDRSIZE);
        nrf24l01_set_rx_addr(i, addrs_rx[i]);
    }
    memcpy_P(addr_tx, NRF24L01_ADDR_TX, NRF24L01_ADDRSIZE);
    nrf24l01_set_tx_addr(addr_tx);

    set_rx();
#if NRF_DEBUG > 0
      dump_regs();
#endif
    console_puts_P(STR_NRF_INITIALIZED);
    return 0;
}

unsigned char nrf24l01_read_ready(unsigned char *pipe) {
    union reg_status status;

    nrf24l01_status(&status);
    if(status.bits.rx_dr) 
    {
        /*get the pipe number*/
        if(pipe)
            *pipe = status.bits.rx_p_no;
    }
    return status.bits.rx_dr;
}

void nrf24l01_read(unsigned char *data) {
    unsigned char i = 0;
    union reg_status status;
    nrf_csn_lo();
    spi_send_receive(NRF24L01_CMD_R_RX_PAYLOAD);
    for(i=0; i<NRF24L01_PAYLOAD; i++)
        *(data++) = spi_send_receive(NRF24L01_CMD_NOP);
    nrf_csn_hi(); 
    /*reset register*/
    status.byte=0;
    status.bits.rx_dr=1;
    reg_write(NRF24L01_REG_STATUS, status.byte);
    /*handle ack payload receipt*/
    nrf24l01_status(&status);
    if (status.bits.tx_ds) {
        status.byte=0;
        status.bits.tx_ds=1;
        reg_write(NRF24L01_REG_STATUS, status.byte);
    }
}

char nrf24l01_write(unsigned char *data) {
    unsigned char i = 0;
    char ret = 0;
    union reg_status status;
    union reg_config conf;

    /*set tx mode*/
    set_tx();

    /*write data*/
    nrf_csn_lo(); /*low CSN*/
    spi_send_receive(NRF24L01_CMD_W_TX_PAYLOAD);
    for (i=0; i<NRF24L01_PAYLOAD; i++)
        spi_send_receive(*(data++));
    nrf_csn_hi(); /*high CSN*/

    /*start transmission*/
    nrf_ce_hi(); /*high CE*/
    _delay_us(15);
    nrf_ce_lo(); /*low CE*/

    /*stop if max_retries reached or send is ok*/
    do {
        _delay_us(10);
        nrf24l01_status(&status);
    }
    while(!(status.bits.max_rt | status.bits.tx_ds));

    if(status.bits.tx_ds)
        ret = 1;

    /*reset PLOS_CNT*/
    reg_write(NRF24L01_REG_RF_CH, NRF24L01_CH);

    /*power down*/
    conf.byte=reg_read(NRF24L01_REG_CONFIG);
    conf.bits.pwr_up=0;
    reg_write(NRF24L01_REG_CONFIG, conf.byte);

    /*set rx mode*/
    set_rx();

    return ret;
}

void nrf24l01_set_rx_addr(unsigned char pipe, unsigned char *addr) {
    unsigned char addr_rev[NRF24L01_ADDRSIZE];
    unsigned char reg_no;

    if(pipe>=6) {
        console_put_msg(STR_NRF_WRONG_PIPE, pipe);
        return;
    }

#if NRF_DEBUG > 0
    console_puts_P(STR_NRF_SET_RX_ADDR);
    console_putd(pipe);
    console_putc('>');
    console_putmem(addr, NRF24L01_ADDRSIZE);
    console_next_line();
#endif

    reg_no=pgm_read_byte(pipes_regs + pipe);
    memcpy(addrs_rx[pipe], addr, NRF24L01_ADDRSIZE); /*cache address*/
    reverse_address(addr, addr_rev);
    if(pipe<2)
        reg_write_seq(reg_no, addr_rev, NRF24L01_ADDRSIZE);
    else
        reg_write(reg_no, addr_rev[0]);
}

void nrf24l01_set_tx_addr(unsigned char *addr) {
    unsigned char addr_rev[NRF24L01_ADDRSIZE];

#if NRF_DEBUG > 0
    console_puts_P(STR_NRF_SET_TX_ADDR);
    console_putmem(addr, NRF24L01_ADDRSIZE);
    console_next_line();
#endif

    memcpy(&addr_tx, addr, NRF24L01_ADDRSIZE); /*cache address*/
    
    reverse_address(addr, addr_rev);
    /*set rx address for ack on pipe 0*/
    reg_write_seq(NRF24L01_REG_RX_ADDR_P0, addr_rev, NRF24L01_ADDRSIZE); 
    /*set tx address*/
    reg_write_seq(NRF24L01_REG_TX_ADDR, addr_rev, NRF24L01_ADDRSIZE); 
}

void nrf24l01_set_tx_addr_pipe(unsigned char pipe) {
    unsigned char addr[NRF24L01_ADDRSIZE];

    if(pipe>=6) {
        console_put_msg(STR_NRF_WRONG_PIPE, pipe);
        return;
    }

    memcpy_P(addr, 
             (PGM_P)pgm_read_word(&(NRF24L01_ADDRS_RX[pipe])), 
             NRF24L01_ADDRSIZE);
    nrf24l01_set_tx_addr(addr);
}

void nrf24l01_print_tx_stat() {
    union reg_observe_tx tx;
    tx.byte = reg_read(NRF24L01_REG_OBSERVE_TX);
    console_puts_P(STR_NRF_TX_STAT);
    console_putd10(tx.bits.plos_cnt);
    console_putc('-');
    console_putd10(tx.bits.arc_cnt);
    console_next_line();
}
