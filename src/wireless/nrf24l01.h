#ifndef _NRF24L01_H_
#define _NRF24L01_H_

#define NRF24L01_PAYLOAD 16

struct nrf24l01_pipe {
    unsigned int p0:1;
    unsigned int p1:1;
    unsigned int p2:1;
    unsigned int p3:1;
    unsigned int p4:1;
    unsigned int p5:1;
    unsigned int unused:2;
} __attribute__ ((packed));

char nrf24l01_init(struct nrf24l01_pipe *pipes, char repeats, char repeat_delay);
unsigned char nrf24l01_read_ready();
void nrf24l01_read(unsigned char *data);
char nrf24l01_write(unsigned char *data);
void nrf24l01_set_rx_addr(unsigned char pipe, unsigned char *addr);
void nrf24l01_set_tx_addr(unsigned char *addr);
void nrf24l01_set_tx_addr_pipe(unsigned char pipe);
void nrf24l01_print_tx_stat();
#endif /*_NRF24L01_H_*/
