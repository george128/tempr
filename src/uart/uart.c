#include <platform/utils.h>
#include <utils/strconv.h>
#include <console/console.h>

#include "uart.h"

#define DEBUG_UART 0


#if DEBUG_UART > 0
PROGMEM static const char STR_REGISTER_UART[]="Register uart: ";
PROGMEM static const char STR_READ_LINE[]="Read line: ";
#endif
#if DEBUG_UART > 1
PROGMEM static const char STR_READ_CHAR[]="Read char: ";
#endif

static struct uart_dev uarts[MAX_UARTS];
static unsigned char uart_idx;

void uart_init()
{
    uart_idx = 0;
    debug_puts("uart_init: started\n");
}

char register_uart(const struct uart_dev *dev)
{
    char cur_idx = uart_idx;
    if(uart_idx == MAX_UARTS) {
        debug_puts("register uart: stack overflow!!!\n");
        return -1;
    }
    memcpy_P(uarts+uart_idx,dev,sizeof(*dev));
    uarts[uart_idx].init();
    uart_idx++;
#if DEBUG_UART > 0
    console_puts_P(STR_REGISTER_UART);
    console_putd10(cur_idx);
    console_putc(' ');
    if(dev->name != NULL) {
        console_puts_P(dev->name);
    }
    console_next_line();
#endif
    return cur_idx;
}

void uart_putc(char uart_no, char ch)
{
    void (*func)(char c);

    func=uarts[uart_no].putc;
    if(func != NULL) {
        func(ch);
    }
}

char uart_read(char uart_no, char timeout_ms)
{
    char (*func_read)();
    char (*func_check)(char);

    func_check=uarts[uart_no].input_check;
    if(func_check == NULL || func_check(timeout_ms)) {
        func_read=uarts[uart_no].readc;
        if(func_read != NULL) {
            return func_read();
        }
    }
    return -1;
}

char uart_read_line(char uart_no, char *buf, char buf_len)
{
    char *ptr=buf;
    char ch, i;
    
    for(ch=-1, i=0; ch!='\n' && i<buf_len-1; ch=uart_read(uart_no, 0)) {
#if DEBUG_UART > 1
        if(ch != -1)
            console_put_msg(STR_READ_CHAR, ch);
#endif
        if(ch != -1 && ch != '\n' && ch != '\r') {
            *(ptr++)=ch;
            i++;
        }
    }
    *ptr=0;
#if DEBUG_UART > 0
    console_put_msg(STR_READ_LINE, i);
    if(i>0) {
        console_puts(buf);
        console_next_line();
    }
#endif
    return i;
}


void uart_puts(char uart_no, const char *str)
{
    char ch;
    void (*func)(char c);

    func=uarts[uart_no].putc;
    if(!func)
        return;
    while((ch=*(str++))!=0)
        func(ch);
}

void uart_put_mem(char uart_no, const char *buf, char len)
{
    void (*func)(char c);

    func=uarts[uart_no].putc;
    if(!func)
        return;
    for(;len>=0;len--,buf++)
        func(*buf);
}

void uart_puts_P(char uart_no, const char *str)
{
    char ch;
    void (*func)(char c);

    func=uarts[uart_no].putc;
    if(!func)
        return;
    while((ch=pgm_read_byte(str++))!=0)
        func(ch);
}

void uart_putd(char uart_no, char byte)
{
    char str[3];
    uart_puts(uart_no, byte2str(byte, str));
}

void uart_putd10(char uart_no, char d)
{
    char buf[4];
    uart_puts(uart_no, byte10_2_str(d, buf));
}

void uart_put_short(char uart_no, unsigned short d)
{
    char buf[6];
    uart_puts(uart_no, short10_2_str(d, buf));
}

void uart_put_long(char uart_no, unsigned long d)
{
    char buf[20];
    uart_puts(uart_no, long10_2_str(d, buf));
}
