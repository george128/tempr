#define NUM_ONEWIRE_SENSORS 5

#include <platform/utils.h>
#include <platform/systime.h>
#include <platform/hw_uart.h>
#include <uart/uart.h>
#include <console/console.h>
#include <timer/timer.h>
#include <onewire/onewire.h>
#include <onewire/ds18b20.h>
#include <wifi/esp8266.h>
#include <7seg/7seg.h>
#include <utils/sleep.h>

#include <string.h>

#define OUTPORTDIR DDRD
#define OUTPORT PORTD
#define OUTPIN 3 

static PROGMEM const char STR_HELLO[]="Hello!!!\n";
static PROGMEM const char STR_NO_SENSORS[]="No sensors found!!!\n";
static PROGMEM const char STR_FIND_START[]="Starting search DS18B20 sensors\n";
static PROGMEM const char STR_FIND_DEVS[]="Found DS18B20 sensors: ";

#define IP_ADDR_LEN 16
#define IP_PORT_LEN 6
#define PREFIX_LEN 8
#define SEG_BUF_LEN (16+6)

static EEMEM const char EEPROM_IP_ADDR[IP_ADDR_LEN] = "192.168.254.255";
static EEMEM const char EEPROM_IP_PORT[IP_PORT_LEN] = "8888";
static EEMEM const char EEPROM_PREFIX[PREFIX_LEN] = "T1";

static struct {
    char num_devs;
    struct ds18b20_mem tempr_mem;
    struct ds18b20_proc tempr_proc;
    struct onewire_dev tempr_rom;
    struct timer t;
    struct esp8266_state wifi_state;
    struct esp8266_read_state wifi_read_state;
    char prefix[PREFIX_LEN];
    char seg_buf[SEG_BUF_LEN];
    char ip_len;
} shm;

static void led_init() 
{
    OUTPORT&=~_BV(OUTPIN);
    OUTPORTDIR|=_BV(OUTPIN);
}

static void led_on() 
{
    OUTPORT|=_BV(OUTPIN);
}

static void led_off()
{
    OUTPORT&=~_BV(OUTPIN);
}

static void temperature_callback(struct ds18b20_conf *conf) {
    char buf[DS18B20_TEMP_2_STR_LEN_MAX+PREFIX_LEN], *ptr;
    char len;

    strncpy(buf, shm.prefix, PREFIX_LEN);
    ptr=buf;
    while(*ptr)ptr++;
    *(ptr++)=':';
    ds18b20_temp2str(ptr, &conf->mem->temperature);
    strncpy(shm.seg_buf+shm.ip_len,ptr,6);
    console_puts(buf);
    console_next_line();
    console_puts(shm.seg_buf);
    console_next_line();

    while(*ptr)ptr++;
    *(ptr++)='\n';
    *(ptr++)=0;
    len=strlen(buf)+1;
    esp8266_conn_send(&shm.wifi_state,buf, len);
}

static void find_sensors() {
    struct onewire_dev onewire_devs[NUM_ONEWIRE_SENSORS];
    char i;
    char buf[18];

    console_puts_P(STR_FIND_START);
    shm.num_devs = onewire_search_rom(onewire_devs, NUM_ONEWIRE_SENSORS);
    console_put_msg(STR_FIND_DEVS, shm.num_devs);

    if(shm.num_devs == 0) {
        console_puts_P(STR_NO_SENSORS);
        return;
    }

    ds18b20_read_conf(&shm.tempr_proc.conf);

    for(i=0;i<shm.num_devs;i++) {
        memcpy(&shm.tempr_rom, &onewire_devs[i], 
               sizeof(onewire_devs[i]));
        console_putd(i);
        console_putc(':');
        console_putc(' ');
        console_puts(onewire2str(buf, &shm.tempr_rom));
        console_next_line();
    }
}

static void sensors_timer_callback(unsigned char timer_id, void *data) {
    if(shm.num_devs == 0) {
        find_sensors();
        return;
    }
    ds18b20_start_temp(&shm.tempr_proc, 0);
}

static void read_cb(char *data, char data_len)
{
    data[data_len]=0;
    console_puts(data);
}


int main(int argc, char *argv[])
{
    char hw_uart_no;
    struct esp8266_stat ip_stat;
    struct _7seg_show _7seg_show;

    eeprom_read_block(shm.prefix, EEPROM_PREFIX, PREFIX_LEN);

    uart_init();
    console_init();
    hw_uart_register(&hw_uart_no, 1);
    systime_init();
    led_init();
    _7seg_init();
    esp8266_init(&shm.wifi_state, hw_uart_no);

 esp8266_rep:
    led_on();
    esp8266_rst(&shm.wifi_state);
    sleep_s(4);
    led_off();
    if(esp8266_get_ip(&shm.wifi_state, &ip_stat) != ESP_8266_STAT_OK)
        goto esp8266_rep;
    
    shm.ip_len=strlen(ip_stat.ip);
    if(shm.ip_len>15)
        shm.ip_len=15;
    strncpy(shm.seg_buf,ip_stat.ip, shm.ip_len);
    shm.seg_buf[shm.ip_len++]='-';
    shm.seg_buf[shm.ip_len]=0;
    
    _7seg_start(&_7seg_show, shm.seg_buf);

    {
        char ip_addr[IP_ADDR_LEN];
        char ip_port[IP_PORT_LEN];
        eeprom_read_block(ip_addr, EEPROM_IP_ADDR, IP_ADDR_LEN);
        eeprom_read_block(ip_port, EEPROM_IP_PORT, IP_PORT_LEN);

        esp8266_conn_open(&shm.wifi_state, &shm.wifi_read_state, 
                          ip_addr, ip_port, read_cb);
    }

    memset(&shm.tempr_proc, 0, sizeof(shm.tempr_proc));
    memset(&shm.tempr_mem, 0, sizeof(shm.tempr_mem));
    shm.tempr_proc.conf.rom=&shm.tempr_rom;
    shm.tempr_proc.conf.mem=&shm.tempr_mem;
    shm.tempr_proc.callback=temperature_callback;

    shm.num_devs=0;
    shm.t.period=10000;
    shm.t.callback=sensors_timer_callback;
    timer_setup(&shm.t);

    for(;;) {
        timer_task();
    }
}
