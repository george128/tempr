local socket = require "socket"
udp = socket.udp()
assert(udp:setsockname('*', 8888))
while true do
    local msg, ip, port = udp:receivefrom()
    local handle = io.popen("date")
    local result = handle:read("*a")
    handle:close()
    io.write(result, ip, " -> ", msg)
end