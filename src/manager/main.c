#include <platform/hw_uart.h>
#include <uart/uart.h>
#include <platform/systime.h>
#include <platform/utils.h>
#include <platform/i2c.h>
#include <utils/strconv.h>
#include <console/console.h>
#include <lcdchar/lcdchar.h>
#include <wifi/esp8266.h>
#include <utils/sleep.h>
#include <timer/timer.h>

#include <string.h>

PROGMEM const char STR_HELLO[] ="Tempr. manager";
PROGMEM const char STR_EXIT_MAIN[] ="Exit main func\n";
PROGMEM const char STR_NO_AP[] ="NO HotSpot";
PROGMEM const char STR_NO_IP[] ="NO IP";
PROGMEM const char STR_WIFI_NO_STATE[] ="Unknown WiFi:";
PROGMEM const char STR_READ_DATA_LEN[] ="Read data len=";

#define IP_ADDR_LEN 16
#define IP_PORT_LEN 6

static EEMEM const char EEPROM_IP_ADDR[IP_ADDR_LEN] = "192.168.254.255";
static EEMEM const char EEPROM_IP_PORT[IP_PORT_LEN] = "8888";

static short cnt[2];
static unsigned short last_time[2];

static void read_cb(char *data, char data_len)
{
    char buf[6];
    unsigned short diff;
    char idx, i;

    idx = data[0]=='T' && data[1]=='1' ? 0:1;
    diff = get_systime() - last_time[idx];
    last_time[idx] = get_systime();

    console_puts_P(STR_READ_DATA_LEN);
    console_putd10(data_len);
    console_putc(':');
    console_putd(idx);
    console_next_line();
    data[data_len]=0;
    console_puts(data);
    cnt[idx]++;
    if(cnt[idx]==10000)
        cnt[idx]=0;
    //lcd_char_clear();
	lcd_char_set_cursor(0, idx);
    for(i=0;i<16;i++)
        lcd_char_write(' ');
	lcd_char_set_cursor(0, idx);
    lcd_char_write_str(short10_2_str(cnt[idx], buf));
    lcd_char_write(' ');
    lcd_char_write_str(data);
    //lcd_char_write(' ');
    lcd_char_write_str(short10_2_str(diff/1000, buf));
}

int main(int argc, char *argv[]) {
    struct esp8266_state wifi_state;
    struct esp8266_read_state wifi_read_state;
    char hw_uart_no;

    cnt[0]=cnt[1]=0;
    last_time[0]=last_time[1]=get_systime();

    uart_init();
    console_init();
    hw_uart_register(&hw_uart_no, 1);
    systime_init();
    i2c_init();
    lcd_char_init();

    lcd_char_write_str_P(STR_HELLO);
    
    esp8266_init(&wifi_state, hw_uart_no);
 esp8266_rep:
    esp8266_rst(&wifi_state);
    sleep_s(4);
    {
        struct esp8266_stat stat;
        char ret=esp8266_get_ip(&wifi_state, &stat);
        switch(ret) {
            case ESP_8266_STAT_NO_AP:
                lcd_char_write_str_P(STR_NO_AP);
                goto esp8266_rep;
            case ESP_8266_STAT_NO_IP:
                lcd_char_write_str_P(STR_NO_IP);
                goto esp8266_rep;
            case ESP_8266_STAT_OK:
                lcd_char_clear();
                lcd_char_set_cursor(0, 0);
                lcd_char_write_str(stat.ap);
                lcd_char_set_cursor(0, 1);
                lcd_char_write_str(stat.ip);
                break;
            default:
                lcd_char_set_cursor(0, 0);
                lcd_char_write_str_P(STR_WIFI_NO_STATE);
                {
                    char buf[3];
                    lcd_char_write_str(byte2str(ret, buf));
                    return 0;
                }
        }
    }
    sleep_s(2);
    {
        char ip_addr[IP_ADDR_LEN];
        char ip_port[IP_PORT_LEN];
        eeprom_read_block(ip_addr, EEPROM_IP_ADDR, IP_ADDR_LEN);
        eeprom_read_block(ip_port, EEPROM_IP_PORT, IP_PORT_LEN);

        esp8266_conn_open(&wifi_state, &wifi_read_state, 
                          ip_addr, ip_port, read_cb);
    }

    for(;;) {
        timer_task();
    }

    console_puts_P(STR_EXIT_MAIN);
    return 0;
}
