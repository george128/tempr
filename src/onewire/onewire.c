#include <platform/onewirehw.h>
#include <platform/utils.h>
#include <console/console.h>
#include <utils/strconv.h>

#include "onewire.h"

#define DEB_PREFIX "onewire: "

PROGMEM static const char STR_ONEWIRE_NO_PRESENCE[]="No any 1-wire device present\n";

#define do_reset(ret) if(!onewirehw_reset()) {     \
        console_puts_P(STR_ONEWIRE_NO_PRESENCE);   \
        return ret;                                \
}

#define ONEWIRE_DEBUG 0

#if ONEWIRE_DEBUG > 0
PROGMEM static const char STR_ONEWIRE_RESET[]="1-wire reset: ";
PROGMEM static const char STR_ONEWIRE_WRITE[]="1-wire wb=";
PROGMEM static const char STR_ONEWIRE_READ[]="1-wire rb=";
PROGMEM static const char STR_ONEWIRE_STABLE_0[]="1-wire search: stable 0 i=";
PROGMEM static const char STR_ONEWIRE_STABLE_1[]="1-wire search: stable 1 i=";
PROGMEM static const char STR_ONEWIRE_LAST_DIFF_1[]="1-wire search: last diff 1 i=";
PROGMEM static const char STR_ONEWIRE_NEW_DIFF[]="1-wire search: new diff=";
PROGMEM static const char STR_ONEWIRE_NEW_DIFF2[]=" i=";
PROGMEM static const char STR_ONEWIRE_SEARCH_FAILED[]="1-wire search: failed i=";
PROGMEM static const char STR_ONEWIRE_SEARCH_ROM_BYTE1[]="1-wire search: rom_byte[";
PROGMEM static const char STR_ONEWIRE_SEARCH_ROM_BYTE2[]="]=";
PROGMEM static const char STR_ONEWIRE_SEARCH_ROM_I1[]="1-wire search: i=";
PROGMEM static const char STR_ONEWIRE_SEARCH_ROM_I2[]=" rom_byte=";
PROGMEM static const char STR_ONEWIRE_SEARCH_ROM_I3[]=" mask=";
#endif

unsigned char onewire_reset()
{
    unsigned char res = onewirehw_reset();
#if ONEWIRE_DEBUG > 0
    console_put_msg(STR_ONEWIRE_RESET, res);
#endif
    return res;
}

static unsigned char do_search_rom(unsigned char *buf, unsigned char last_diff_bit)
{
    unsigned char bit1, bit2, 
        mask=ONEWIRE_MASK(0),
        rom_byte=0,
        new_diff_bit=0;

    onewire_write_byte(ONEWIRE_SEARCHROM);

    for(unsigned char i=0;i<64;i++) {
        bit1=onewirehw_read_bit();
        bit2=onewirehw_read_bit();

        if(bit1 && bit2) {
#if ONEWIRE_DEBUG > 1
            console_put_msg(STR_ONEWIRE_SEARCH_FAILED, i);
#endif
            return ONEWIRE_SEARCH_FAILED;
        } else if(bit1 != bit2) {
            if(bit1) {
                rom_byte|=mask;
#if ONEWIRE_DEBUG > 1
                console_put_msg(STR_ONEWIRE_STABLE_1, i);
#endif
            } else {
#if ONEWIRE_DEBUG > 1
                console_put_msg(STR_ONEWIRE_STABLE_0, i);
#endif
            }
        } else {
            if(i == last_diff_bit) {
                rom_byte|=mask;
#if ONEWIRE_DEBUG > 1
                console_put_msg(STR_ONEWIRE_LAST_DIFF_1, i);
#endif
            } else if(i < last_diff_bit) {
                new_diff_bit = i;
#if ONEWIRE_DEBUG > 1
                console_puts_P(STR_ONEWIRE_NEW_DIFF);
                console_putd(new_diff_bit);
                console_put_msg(STR_ONEWIRE_NEW_DIFF2, i);
#endif
            }
        }
#if ONEWIRE_DEBUG > 1
        console_puts_P(STR_ONEWIRE_SEARCH_ROM_I1);
        console_putd(i);
        console_puts_P(STR_ONEWIRE_SEARCH_ROM_I2);
        console_putd(rom_byte);
        console_put_msg(STR_ONEWIRE_SEARCH_ROM_I3, mask);
#endif

        onewirehw_write_bit(rom_byte&mask);

        ONEWIRE_MASK_NEXT(mask);
        if(!mask) {
            unsigned char num=i/8;
            mask=ONEWIRE_MASK(0);
            buf[num]=rom_byte;
#if ONEWIRE_DEBUG > 1
            console_puts_P(STR_ONEWIRE_SEARCH_ROM_BYTE1);
            console_putd(num);
            console_put_msg(STR_ONEWIRE_SEARCH_ROM_BYTE2, rom_byte);
#endif
            rom_byte=0;
        }
    }
    return new_diff_bit;
}

unsigned char onewire_search_rom(struct onewire_dev *devs, unsigned char max_devs)
{
    unsigned char last_diff_bit=64,
        dev_no=0;

    do_reset(0);

    do {
        last_diff_bit = do_search_rom((unsigned char *)&devs[dev_no], 
                                      last_diff_bit);
        debug_print(DEB_PREFIX "last_diff_bit=%d\n", last_diff_bit);
        if(last_diff_bit == ONEWIRE_SEARCH_FAILED) {
            debug_puts(DEB_PREFIX "search failed!\n");
            return 0;
        }
        dev_no++;
    } while(dev_no<max_devs && last_diff_bit != ONEWIRE_SEARCH_COMPLETE);

    return dev_no;
}

unsigned char onewire_match_rom(struct onewire_dev *rom)
{
    char *buf=(char *)rom;

    do_reset(0);

    onewire_write_byte(ONEWIRE_MATCHROM);
    for(unsigned char i=0;i<8;i++)
        onewire_write_byte(buf[i]);

    return 1;
}

unsigned char onewire_skip_rom()
{
    do_reset(0);
    onewire_write_byte(ONEWIRE_SKIPROM);

    return 1;
}

struct onewire_dev* onewire_read_rom(struct onewire_dev *rom)
{
    char *buf=(char *)rom;

    do_reset(NULL);
    onewire_write_byte(ONEWIRE_READROM);
    for(unsigned char i=0;i<8;i++)
        buf[i]=onewire_read_byte();

    return rom;
}

void onewire_write_byte(unsigned char byte)
{
    unsigned char mask=1;
#if ONEWIRE_DEBUG > 0
    console_put_msg(STR_ONEWIRE_WRITE, byte);
    debug_print(DEB_PREFIX "write byte: %02x\n", byte);
#endif
    while(mask!=0) {
        onewirehw_write_bit(byte&mask);
        mask<<=1;
    }
}

unsigned char onewire_read_byte()
{
    unsigned char res=0, mask=1;
    while(mask!=0) {
        if(onewirehw_read_bit())
            res |= mask;
        mask<<=1;
    }
#if ONEWIRE_DEBUG > 0
    debug_print(DEB_PREFIX "read byte: %02x\n", res);
    console_put_msg(STR_ONEWIRE_READ, res);
#endif
    return res;
}

char *onewire2str(char *buf, struct onewire_dev *rom)
{
    char *ptr=buf;
    int i;

    byte2str(rom->crc, ptr);
    ptr+=2;
    for(i=0;i<6;i++) {
        byte2str(rom->serial[i],ptr);
        ptr+=2;
    }
    byte2str(rom->family,ptr);
    return buf;
}
