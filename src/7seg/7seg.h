#ifndef _7SEG_H_
#define _7SEG_H_

#include <timer/timer.h>

struct _7seg_show {
    struct timer t;
    unsigned char timer_id;
    char *str, *ptr;
};

void _7seg_init();
void _7seg_set(unsigned char b);
void _7seg_start(struct _7seg_show *show, char *str);
void _7seg_stop(struct _7seg_show *show);

#endif /*_7SEG_H_*/
