#include <platform/utils.h>
#include <timer/timer.h>

#include "7seg.h"

/*
  3
  _
5|4|2
  -
6| |0
  _ .1
  7
*/


union _7seg_stor {
    unsigned char val;
    struct {
        int c:6;
        int b:2;
    } __attribute__((packed)) port;
};

static PROGMEM const unsigned char _7_seg_transform[] = {
    0xed, 0x05, 0xdc, 0x9d,
    0x35, 0xb9, 0xf9, 0x0d,
    0xfd, 0xbd, 0x7d, 0xf1,
    0xe8, 0xd5, 0xf8, 0x78,
    /*-*/0x10, /*,*/0x02, /* */0x00, /*_*/0x80
};

void _7seg_init()
{
    DDRC=0x3f;
    PORTC &= 0xc0;
    DDRB|=_BV(0)|_BV(1);
    PORTB &= 0xfc;
}

void _7seg_set(unsigned char b)
{
    union _7seg_stor stor;
    stor.val = read_rom(_7_seg_transform+b);
    PORTC &= 0xc0;
    PORTC |= stor.port.c;
    PORTB &= 0xfc;
    PORTB |= stor.port.b;
}

void _7seg_timer_cb(unsigned char timer_id, void *data)
{
    struct _7seg_show *show = (struct _7seg_show *)data;
    unsigned char ch = *show->ptr;
    if(!ch) {
        show->ptr=show->str;
        _7seg_set(0x10);
        return;
    }
    if(ch >= '0' && ch <= '9')
        _7seg_set(ch-'0');
    else if(ch == '-')
        _7seg_set(0x10);
    else if(ch=='.')
        _7seg_set(0x11);
    else if(ch==' ')
        _7seg_set(0x12);
    else if(ch=='_')
        _7seg_set(0x13);

    if(show->ptr != show->str && ch == *(show->ptr-1))
        PORTC |= 2;

    show->ptr++;
}


void _7seg_start(struct _7seg_show *show, char *str)
{
    show->t.period=800;
    show->t.callback=_7seg_timer_cb;
    show->t.data=show;
    show->str=str;
    show->ptr=str;
    show->timer_id=timer_setup(&show->t);
}

void _7seg_stop(struct _7seg_show *show)
{
    timer_free(show->timer_id);
}
