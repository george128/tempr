#ifndef _ESP8266_READ_H_
#define _ESP8266_READ_H_

#include <timer/timer.h>

#define READ_MSG_LEN 16

struct esp8266_read_state {
    char uart_id;
    char state;
    char cmd_prefix[6];
    char data_idx;
    char data[16];
    char data_len;
    void (*read_cb)(char *data, char data_len);
    struct timer t;
    unsigned char timer_id;
};

void esp8266_read_start(char uart_id, 
                        struct esp8266_read_state *read_state,
                        void (*read_cb)(char *data, char data_len));
void esp8266_read_stop(struct esp8266_read_state *read_state);


#endif /*_ESP8266_READ_H_*/
