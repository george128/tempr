#include <uart/uart.h>
#include <platform/utils.h>
#include <platform/systime.h>
#include <platform/wdt.h>
#include <console/console.h>
#include <timer/timer.h>
#include <utils/strconv.h>
#include <utils/strings.h>

#include <string.h>

#include "esp8266.h"

#define ESP_DEBUG 1

PROGMEM static const char EOL[]="\r\n";

PROGMEM static const char CMD_AT[]="AT";
PROGMEM static const char CMD_PLUS_RST[]="RST";
PROGMEM static const char CMD_PLUS_VERSION[]="GMR";
PROGMEM static const char CMD_PLUS_SLEEP[]="GSLP";
PROGMEM static const char CMD_PLUS_GET_IP[]="CIPSTA?";
PROGMEM static const char CMD_PLUS_GET_WIFI_CONN[]="CWJAP?";
PROGMEM static const char CMD_PLUS_CONN_START[]="CIPSTART=\"UDP\",\"";
PROGMEM static const char CMD_PLUS_CONN_STATUS[]="CIPSTATUS";
PROGMEM static const char CMD_PLUS_CONN_SEND[]="CIPSEND=";
PROGMEM static const char CMD_PLUS_CONN_CLOSE[]="CIPCLOSE";
PROGMEM static const char CMD_PLUS_CONN_MULTI[]="CIPMUX";

PROGMEM static const char CMD_RESP_OK[]="OK";
PROGMEM static const char CMD_RESP_SEND_OK[]="SEND OK";
PROGMEM static const char CMD_RESP_ERROR[]="ERROR";
PROGMEM static const char CMD_RESP_READY[]="ready";


#if ESP_DEBUG > 0
PROGMEM static const char STR_USE_UART[] = "ESP: used uart: ";
PROGMEM static const char STR_CMD[] = "ESP: cmd: '";
PROGMEM static const char STR_CMD_DONE[] = "ESP: cmd done: '";
PROGMEM static const char STR_PARAM[] = " param: '";
PROGMEM static const char STR_READ_LINE[] = "ESP: read line ";
PROGMEM static const char STR_READ_ECHO[] = "ESP: read cmd echo\n";
PROGMEM static const char STR_READ_EMPTY[] = "ESP: read empty string\n";
PROGMEM static const char STR_READ_CB_END[] = "ESP: read: cb end\n";
PROGMEM static const char STR_CONSTRUCT_CMD[] = "ESP: cmd: \n";
PROGMEM static const char STR_OPEN_CONN_PARAM[] = "ESP: open conn param: '";
PROGMEM static const char STR_SEND_DATA[] = "ESP: send data: ";
#endif

void esp8266_init(struct esp8266_state *state, char uart_id)
{
    state->uart_id=uart_id;
    console_put_msg(STR_USE_UART, state->uart_id);
}

static char cb_read_rst(char *line, const char *param) 
{
    return !strcmp_P(line, CMD_RESP_READY);
}
    
static char cb_read_ok(char *line, const char *param) 
{
    return !strcmp_P(line, CMD_RESP_OK);
}

char *esp8266_send_cmd(struct esp8266_state *state,
                       const char* cmd_rom, const char *param, 
                       char *buf, char buf_len, char is_line_cb,
                       char (*cb_read)(char *read_line, const char *param)) 
{
    char cmd_len;
    char read_len=0;
    char *ptr=buf, *line_ptr=buf;
    char cmd[32];

    wdt_enable(WDTO_8S);
    
    strncpy_P(cmd, cmd_rom, 32);
    cmd_len=strlen(cmd);
#if ESP_DEBUG > 0
    console_puts_P(STR_CMD);
    console_puts(cmd);
    console_putc('\'');
    console_putc(' ');
    console_putd10(cmd_len);
    if(param) {
        console_puts_P(STR_PARAM);
        console_puts(param);
        console_putc('\'');
    }
    console_next_line();
#endif
    uart_puts_P(state->uart_id, CMD_AT);
    uart_putc(state->uart_id, '+');
    uart_puts(state->uart_id, cmd);
    if(param)
        uart_puts(state->uart_id, param);
    uart_puts_P(state->uart_id, EOL);
    for(;;) {
        char rb;
        if((rb=uart_read(state->uart_id,0))<0)
            continue;
        if(rb=='\r')
            continue;
        if(rb=='\n') {
            *ptr=0;
            if(read_len > 0) {
#if ESP_DEBUG > 2
                console_puts_P(STR_READ_LINE);
                console_putd(read_len);
#endif
#if ESP_DEBUG > 1
                if(read_len <10) {
                    console_putc('\'');
                    console_puts(ptr);
                    console_putc('\'');
                    console_next_line();
                }
#endif
                if(is_line_cb && cb_read(line_ptr, param)) {
#if ESP_DEBUG > 1
                    console_puts_P(STR_READ_CB_END);
#endif
                    break;
                }
            } else {
#if ESP_DEBUG > 1
                    console_puts_P(STR_READ_EMPTY);
#endif
                continue;
            }
            
            line_ptr=ptr+1;
            read_len=0;
        } // rb=='\n'
        *(ptr++)=rb;
        *ptr=0;
        read_len++;
        if(!is_line_cb) {
            if(cb_read(line_ptr, param)) {
#if ESP_DEBUG > 1
                console_puts_P(STR_READ_CB_END);
#endif
                break;
            }
        }
        if(ptr-buf>=buf_len) {
            ptr=buf;
            *buf=0;
        }
    }
#if ESP_DEBUG > 0
            console_puts_P(STR_CMD_DONE);
            console_puts(buf);
            console_putc('\'');
            console_next_line();
#endif
    wdt_reset();
    wdt_disable();
    return buf;
}

void esp8266_rst(struct esp8266_state *state) 
{
    char resp[127];
    
    esp8266_send_cmd(state, CMD_PLUS_RST, NULL, resp, 127, 1, cb_read_rst);
}

char esp8266_get_ip(struct esp8266_state *state,
                    struct esp8266_stat *stat)
{
    char resp[127], *ptr;
    /*
AT+CWJAP?
+CWJAP:"gver2"

OK*/
    esp8266_send_cmd(state, CMD_PLUS_GET_WIFI_CONN, NULL, 
                     resp, 127, 1, cb_read_ok);
    console_puts(resp);
    console_next_line();
    ptr=find_quoted_str(resp);
    if(ptr==NULL)
        return ESP_8266_STAT_NO_AP;
    if(stat!=NULL)
        strncpy(stat->ap, ptr, 16);
    /*
AT+CIPSTA?
+CIPSTA:"192.168.254.112"

OK
*/
    esp8266_send_cmd(state, CMD_PLUS_GET_IP, NULL, resp, 127, 1, cb_read_ok);
    ptr=find_quoted_str(resp);
    if(ptr==NULL)
        return ESP_8266_STAT_NO_IP;
    if(stat!=NULL)
        strncpy(stat->ip, ptr, 16);

    return ESP_8266_STAT_OK;
}

void esp8266_conn_open(struct esp8266_state *state,
                       struct esp8266_read_state *read_state,
                       char *dest_ip, char *port, 
                       void (*read_cb)(char *data, char data_len))
{
    char buf[127];
    char param[40], *pparam=param;

    strcpy(pparam, dest_ip);
    pparam+=strlen(dest_ip);
    *(pparam++)='"';
    *(pparam++)=',';
    *pparam=0;
    strcat(pparam, port);
    pparam+=strlen(port);
    *(pparam++)=',';
    *pparam=0;
    strcat(pparam, port);
    pparam+=strlen(port);
    *(pparam++)=',';
    *(pparam++)='0';
    *pparam=0;
    
#if ESP_DEBUG > 0
    console_puts_P(STR_OPEN_CONN_PARAM);
    console_puts(param);
    console_putc('\'');
    console_next_line();
#endif
    
    esp8266_send_cmd(state, CMD_PLUS_CONN_START, 
                     param, buf, 127, 1, cb_read_ok);
    state->data=read_state;
    if(read_state)
        esp8266_read_start(state->uart_id, read_state, read_cb);
}

void esp8266_conn_close(struct esp8266_state *state)
{
    char buf[127];

    if(state->data)
        esp8266_read_stop((struct esp8266_read_state *)(state->data));
    esp8266_send_cmd(state, CMD_PLUS_CONN_CLOSE, NULL, buf, 127, 1, cb_read_ok);
}

struct send_param {
    char len[4];
    char is_send;
    char *data;
    struct esp8266_state *state;
};

static char cb_read_send_ok(char *line, const char *param) 
{
    struct send_param *send_param = (struct send_param *)param;

    if(*line == '>' && !send_param->is_send) {
        char data_len = str10_2byte(param);

#if ESP_DEBUG > 0
        console_puts_P(STR_SEND_DATA);
        console_putc(' ');
        console_putd(data_len);
        console_putc('-');
        console_put_mem(send_param->data, data_len);
        console_next_line();
#endif
        uart_put_mem(send_param->state->uart_id, send_param->data, data_len);
        send_param->is_send=1;
        return 0;
    } else {
        return strlen(line)==7 && !strcmp_P(line, CMD_RESP_SEND_OK);
    }
}

void esp8266_conn_send(struct esp8266_state *state, 
                       char *data, char data_len)
{
    char buf[127], *ptr;
    struct send_param send_param;

    ptr=byte10_2_str(data_len, buf);
    strncpy(send_param.len,ptr,3);
    send_param.is_send=0;
    send_param.data=data;
    send_param.state = state;

    esp8266_send_cmd(state, CMD_PLUS_CONN_SEND, (char *)&send_param, 
                     buf, 127, 0, cb_read_send_ok);
}
