#include <timer/timer.h>
#include <console/console.h>
#include <platform/utils.h>
#include <utils/strconv.h>

#include "esp8266_read.h"

#define ESP8266_READ_STATE_CMD 0
#define ESP8266_READ_STATE_LEN 1
#define ESP8266_READ_STATE_DAT 2

PROGMEM static const char CMD_PLUS_CONN_READ[]="+IPD,";
PROGMEM static const char STR_UNKNOWN_STATE[] = "ESP READ: unknown state: ";

/*
+IPD,7:12wwee
*/

static void process_cmd(char ch, struct esp8266_read_state *state) {
    char prefix_idx = state->data_idx;
    char *ptr = state->cmd_prefix + prefix_idx;
    
    if(ch != *ptr) {
        state->data_idx = 0;
        return;
    }
#if ESP_DEBUG > 0
        console_putc(ch);
#endif
    ptr++;
    if(*ptr) {
        state->data_idx=prefix_idx+1;
    } else {
        state->state=ESP8266_READ_STATE_LEN;
        state->data_len=0;
        state->data_idx=0;
    }
}

static void process_len(char ch, struct esp8266_read_state *state) {
    char data_idx = state->data_idx;
    char *ptr = state->data;
    if(ch==':') {
        ptr[data_idx]=0;
#if ESP_DEBUG > 1
        console_next_line();
        console_putc('d');
        console_puts(ptr);
#endif
        state->data_len=str10_2byte(ptr);
        state->data_idx=0;
        state->state=ESP8266_READ_STATE_DAT;
#if ESP_DEBUG > 0
        console_next_line();
        console_putc('D');
        console_putd(state->data_len);
#endif        
    } else {
        ptr[data_idx]=ch;
        state->data_idx++;
    }
}

static void process_data(char ch, struct esp8266_read_state *state) {
    char data_idx = state->data_idx;
    char *ptr = state->data+data_idx;
    
    *ptr=ch;
    if(data_idx+1==state->data_len) {
#if ESP_DEBUG > 0
        console_next_line();
        console_putc('D');
        console_putc('E');
        console_next_line();
        console_put_mem(state, sizeof(*state));
        console_next_line();
#endif        
        if(state->read_cb != NULL) {
            state->read_cb(state->data, state->data_len);
#if ESP_DEBUG > 0
        console_putc('C');
        console_putc('E');
        console_next_line();
#endif        
        }
#if ESP_DEBUG > 0
        console_putc('D');
        console_putc('D');
        console_next_line();
#endif        
        state->data_idx=0;
        state->data_len=0;
        state->state=ESP8266_READ_STATE_CMD;
    } else {
        state->data_idx++;
    }
}

static void esp8266_read_task(unsigned char timer_id, void *data) {
    char ch;
    struct esp8266_read_state *state = (struct esp8266_read_state *)data;

    while((ch = uart_read(state->uart_id, 1))!=-1) {

        switch(state->state) {
            case ESP8266_READ_STATE_CMD:
                process_cmd(ch, state);
                break;
            case ESP8266_READ_STATE_LEN:
                process_len(ch, state);
                break;
            case ESP8266_READ_STATE_DAT:
                process_data(ch, state);
                break;
            default:
                console_put_msg(STR_UNKNOWN_STATE, state->state);
                return;
        }
    }    
}

void esp8266_read_start(char uart_id, 
                        struct esp8266_read_state *read_state,
                        void (*read_cb)(char *data, char data_len))
{
    read_state->uart_id = uart_id;
    read_state->read_cb=read_cb;
    strcpy_P(read_state->cmd_prefix, CMD_PLUS_CONN_READ);
    read_state->data_idx=0;
    read_state->data_len=0;
    read_state->state = ESP8266_READ_STATE_CMD;
    read_state->t.period=0;
    read_state->t.callback=esp8266_read_task;
    read_state->t.data = read_state;

    read_state->timer_id=timer_setup(&read_state->t);
}

void esp8266_read_stop(struct esp8266_read_state *read_state)
{
    timer_free(read_state->timer_id);
    read_state->read_cb=NULL;
    read_state->data_idx=0;
    read_state->data_len=0;
    read_state->state = ESP8266_READ_STATE_CMD;
}
