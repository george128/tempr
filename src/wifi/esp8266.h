#ifndef _ESP_8266_H_
#define _ESP_8266_H_

#include "esp8266_read.h"

struct esp8266_stat {
    char ap[16];
    char ip[16];
};

struct esp8266_state {
    char uart_id;
    void *data;
};

#define ESP_8266_STAT_NO_AP -1
#define ESP_8266_STAT_NO_IP -2
#define ESP_8266_STAT_OK 0

void esp8266_init(struct esp8266_state *state, char uart_id);
void esp8266_rst(struct esp8266_state *state);
char esp8266_get_ip(struct esp8266_state *state, struct esp8266_stat *stat);
void esp8266_conn_open(struct esp8266_state *state,
                       struct esp8266_read_state *read_state,
                       char *dest_ip, char *port, 
                       void (*read_cb)(char *data, char data_len));
void esp8266_conn_close(struct esp8266_state *state);
void esp8266_conn_send(struct esp8266_state *state, char *data, char data_len);


#endif /*_ESP_8266_H_*/
