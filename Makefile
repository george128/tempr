AVR_MCU=atmega328p
F_CPU=16000000UL

OBJS_COMMON=console/console.o \
    uart/uart.o \
    utils/strconv.o \
    utils/sleep.o \
    utils/utils.o \
    utils/strings.o \
    timer/timer.o \
    platform/hw_uart.o \
    platform/soft_uart.o \
    platform/console.o \
    platform/systime.o \
    platform/spi.o \
    wifi/esp8266.o \
    wifi/esp8266_read.o
OBJS_SENSOR=$(OBJS_COMMON) \
    platform/onewirehw.o \
    onewire/onewire.o \
    onewire/ds18b20.o \
	7seg/7seg.o \
    sensor/main.o
OBJS_MANAGER=$(OBJS_COMMON) \
    lcdchar/lcdchar.o \
    platform/i2c.o \
    manager/main.o

SRCDIR=src
BINDIR=bin
TOOLDIR=~/git/aquacontroller/soft/firmware/tools

CFLAGS=-mmcu=$(AVR_MCU) -DF_CPU=$(F_CPU) \
    -I$(SRCDIR) \
    -O2 -g -Wall -pedantic -std=c99 -Wno-char-subscripts -fdata-sections -ffunction-sections
LDFLAGS=-mmcu=$(AVR_MCU) -Wl,--gc-sections
LIBS=
CC=$(TOOLDIR)/bin/avr-gcc
RANLIB=$(TOOLDIR)/bin/avr-ranlib
OBJCOPY=$(TOOLDIR)/bin/avr-objcopy
SIZE=$(TOOLDIR)/bin/avr-size

all: sensor.elf manager.elf

sensor.elf: $(patsubst %,$(BINDIR)/%,$(OBJS_SENSOR))
	$(CC) $(LDFLAGS) $^ $(LIBS) -o $(BINDIR)/$@
	$(OBJCOPY) -R .eeprom -O binary \
	    $(BINDIR)/$@ $(BINDIR)/$(subst .elf,.bin,$@)
	$(OBJCOPY) -j .eeprom --no-change-warnings --change-section-lma .eeprom=0 -O binary \
	    $(BINDIR)/$@ $(BINDIR)/$(subst .elf,.eeprom,$@)
	$(SIZE) --format=SysV $(BINDIR)/$@

prog_sensor: 
	avrdude -c usbasp -p m328p -U flash:w:$(BINDIR)/sensor.bin

prog_sensor_eeprom:
	avrdude -c usbasp -p m328p -U eeprom:w:$(BINDIR)/sensor.eeprom:r

manager.elf: $(patsubst %,$(BINDIR)/%,$(OBJS_MANAGER))
	$(CC) $(LDFLAGS) $^ $(LIBS) -o $(BINDIR)/$@
	$(OBJCOPY) -R .eeprom -O binary \
	    $(BINDIR)/$@ $(BINDIR)/$(subst .elf,.bin,$@)
	$(OBJCOPY) -j .eeprom --no-change-warnings --change-section-lma .eeprom=0 -O binary \
	    $(BINDIR)/$@ $(BINDIR)/$(subst .elf,.eeprom,$@)
	$(SIZE) --format=SysV $(BINDIR)/$@

prog_manager_eeprom:
	avrdude -c usbasp -p m328p -U eeprom:w:$(BINDIR)/manager.eeprom:r

prog_manager: 
	avrdude -c usbasp -p m328p -U flash:w:$(BINDIR)/manager.bin


$(BINDIR)/%.o: $(SRCDIR)/%.c
	mkdir -pv $(dir $@)
	$(CC) -c $(CFLAGS) $< -o $@

%.a:
	$(MAKE) -C $(dir $(subst $(BINDIR),$(SRCDIR),$@)) \
	    BINDIR=$(PWD)/$(BINDIR) \
	    RANLIB=$(RANLIB) \
		CFLAGS="$(subst -I,-I../../,$(CFLAGS))"

$(BINDIR):
	mkdir -p $(BINDIR)

clean: 
	rm -rf $(BINDIR)

